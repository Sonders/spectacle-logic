/**
 * @desc Директор. Выполняет роль управления актерами. Раздает команды.
 * @constructor
 * @param {Scene} scene сцена
 */
let Director = function (scene) {
    this.scene = scene
}

export default Director