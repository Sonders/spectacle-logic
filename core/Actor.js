import Scenario from './Scenario'

/**
 * @desc Актер. Исполняет роль динамичного компонента сайта. Может менять свое поведение исходя из сценария.
 * @constructor
 * @param {string} name имя актера
 */
let Actor = function (name) {
    this.name = name
    this.scenario = new Scenario(null, {
    	data: [],
    	headers: []
    })
    this.updateMethod = null
}

/**
 * @desc Задать имя актера
 * @param {string} name имя актера
 */
Actor.prototype.setName = function (name) {
    this.name = name
}

/**
 * @desc Получить имя актера
 * @returns {string} name имя актера
 */
Actor.prototype.getName = function () {
    return this.name
}

/**
 * @desc Задать сценарий для актера
 * @param {Scenario} scenario сценарий
 */
Actor.prototype.setScenario = function (scenario) {
    this.scenario = scenario

    if (this.updateMethod !== null) {
        this.updateMethod()
    }
}

/**
 * @desc Получить сценарий актера
 * @returns {Scenario} scenario сценарий актера
 */
Actor.prototype.getScenario = function () {
    return this.scenario
}

Actor.prototype.setUpdateMethod = function (updateMethod) {
    this.updateMethod = updateMethod
}

export default Actor