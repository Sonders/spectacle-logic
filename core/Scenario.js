/**
 * @desc Сценарий. Необходим для задания поведения актеру.
 * @constructor
 * @param {string} name название сценария
 * @param {object} story собственно история сценария. Набор данных
 */
let Scenario = function (name, story) {
    this.name = name
    this.story = story
}

/**
 * @desc Получить название сценария
 * @returns {string} name название сценария
 */
Scenario.prototype.getName = function () {
    return this.name
}

/**
 * @desc Получить историю сценария
 * @returns {object} story история сценария
 */
Scenario.prototype.getStory = function() {
    return this.story
}

export default Scenario