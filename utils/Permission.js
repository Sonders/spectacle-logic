let Permission = function() {
	this.role = null
	this.accesses = {}
	this.accessSuccessHandler = null
	this.accessFailedHandler = null
	this.tokenExistsHandler = null
	this.tokenIsValidHandler = null
}

Permission.prototype.setAccessHandlers = function (successHandler, failedHandler) {
	this.accessSuccessHandler = successHandler
	this.accessFailedHandler = failedHandler
}

Permission.prototype.setTokenHandlers = function (tokenExistsHandler, tokenIsValidHandler) {
	this.tokenExistsHandler = tokenExistsHandler
	this.tokenIsValidHandler = tokenIsValidHandler
}

Permission.prototype.setRole = function (role) {
	this.role = role
}

Permission.prototype.setAccess = function (params, role) {
	if (params.isAuthenticated !== undefined) {
		let tokenExists = this.tokenExistsHandler()

		if (!tokenExists) {
			return this.accessFailedHandler("Token doesn't exists")
		}

		let tokenIsValid = this.tokenIsValidHandler()

		if (!tokenIsValid) {
			return this.accessFailedHandler("Token isn't valid")
		}
	}

	if (role !== undefined) {
		if (this.role !== role) {
			return this.accessFailedHandler("Your person isn't " + this.role)
		}
	}

	return this.accessSuccessHandler()
}

Permission.instance = new Permission()

Permission.getInstance = function() {
	return Permission.instance
}

export default Permission