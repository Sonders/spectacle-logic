import Scene from '../core/Scene'

/**
 * @desc Кастомная реактовская сцена
 * @constructor
 * @param {string} name имя сцены
 */
let ReactScene = function() {
    Scene.call(this)

    this.screens = []
}

ReactScene.prototype = Object.create(Scene.prototype)

/**
 * @desc Добавление экрана для обновления
 * @param {string} name название экрана
 * @param {object} screen экран
 */
ReactScene.prototype.addScreen = function (name, screen) {
    this.screens.push({ name: name, screen: screen })
}

/**
 * @desc Обновление всех экранов
 */
ReactScene.prototype.update = function () {
    Scene.prototype.update.call(this)

    let screens = ReactScene.getInstance().screens

    screens.forEach(screenObject => {
        screenObject.screen.forceUpdate()
    })
}

ReactScene.instance = new ReactScene()

ReactScene.getInstance = function() {
    return ReactScene.instance
}

export default ReactScene